var data = {
    footer: {
        defaults: {
            image: "/static/img/content/shared/mondelez-logo.svg",
            copyright: "© 2020 Mondelez International group - Alle rechten voorbehouden\n",
            links: [
                {
                    link: "/terms",
                    text: "Gebruikersvoorwaarden"
                },
                {
                    link: "https://eu.mondelezinternational.com/privacy-notice?sc_lang=nl-NL&amp;siteId=rNbaVYBMAj9UahTSVdu1Aw%3d%3d",
                    text: "Privacyverklaring"
                },
                {
                    link: "/cookies",
                    text: "Cookiebeleid"
                },
                {
                    link: "https://contactus.mdlzapps.com/form?siteId=Oz9901fSfty03Bf3iVZfDg%3D%3D",
                    text: "Contact"
                },
            ]
        }
    }
};