var data = {
    products: {
        defaults: {
            header: "Jouw persoonlijke packs",
            productsList: [
                {
                    text: "LU Cent",
                    image: "/static/img/content/packs/pack-1.png"
                },
                {
                    text: "LU Véritable Petit Beurre",
                    image: "/static/img/content/packs/pack-2.png"
                },
                {
                    text: "LU Bastogne",
                    image: "/static/img/content/packs/pack-3.png"
                },
                {
                    text: "LU PIM’s",
                    image: "/static/img/content/packs/pack-4.png"
                },
                {
                    text: "LU Petit Beukelaer",
                    image: "/static/img/content/packs/pack-5.png"
                },
                {
                    text: "LU Petit Beukelaer Familie",
                    image: "/static/img/content/packs/pack-6.png"
                },
                {
                    text: "LU Cha-Cha",
                    image: "/static/img/content/packs/pack-7.png"
                },
            ]
        }
    }
};