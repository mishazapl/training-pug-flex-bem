/* Module data structure */

// moduleName: {
//     dataType: {
//         property: value
//     }
// }

/* Module data example */

_template: {
    big: {
        title: 'Hello world',
        age: 10,
        button: false
    }
},

'footer': {
        defaults: {
            image: "/static/img/content/shared/mondelez-logo.svg",
            copyright: "© 2020 Mondelez International group - Alle rechten voorbehouden\n",
            links: [
                {
                    link: "/terms",
                    text: "Gebruikersvoorwaarden"
                },
                {
                    link: "https://eu.mondelezinternational.com/privacy-notice?sc_lang=nl-NL&amp;siteId=rNbaVYBMAj9UahTSVdu1Aw%3d%3d",
                    text: "Privacyverklaring"
                },
                {
                    link: "/cookies",
                    text: "Cookiebeleid"
                },
                {
                    link: "https://contactus.mdlzapps.com/form?siteId=Oz9901fSfty03Bf3iVZfDg%3D%3D",
                    text: "Contact"
                },
            ]
        }
    }
,

'formInput': {
        defaults: {
            mainLabel: "Typ hier jouw koosnaampje",
            bgImage: "/static//img/content/decor/input-pack.png"
        }
    }
,

'head': {
    defaults: {
        title: 'default title',
        useSocialMetaTags: true
    }
},

'header': {
        defaults: {
            languages: ['NL','FR'],
            logoSrc: "/static/img/content/shared/lu-logo.svg"
        }
    }
,

'products': {
        defaults: {
            header: "Jouw persoonlijke packs",
            productsList: [
                {
                    text: "LU Cent",
                    image: "/static/img/content/packs/pack-1.png"
                },
                {
                    text: "LU Véritable Petit Beurre",
                    image: "/static/img/content/packs/pack-2.png"
                },
                {
                    text: "LU Bastogne",
                    image: "/static/img/content/packs/pack-3.png"
                },
                {
                    text: "LU PIM’s",
                    image: "/static/img/content/packs/pack-4.png"
                },
                {
                    text: "LU Petit Beukelaer",
                    image: "/static/img/content/packs/pack-5.png"
                },
                {
                    text: "LU Petit Beukelaer Familie",
                    image: "/static/img/content/packs/pack-6.png"
                },
                {
                    text: "LU Cha-Cha",
                    image: "/static/img/content/packs/pack-7.png"
                },
            ]
        }
    }
,

__iconsData: {
    
        'facebook': {
            width: '96.124px',
            height: '96.123px'
        },
    
        'scroll-arrow': {
            width: '276.76px',
            height: '96.38px'
        },
    
},

__pages: [{
                name: 'index',
                href: 'index.html'
             }]